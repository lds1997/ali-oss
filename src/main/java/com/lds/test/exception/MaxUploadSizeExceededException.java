package com.lds.test.exception;

/**
 * @Author lds
 * @Date 2022/08/03 15:03
 * @Describe
 **/

public class MaxUploadSizeExceededException extends RuntimeException{
    private String module;

    public MaxUploadSizeExceededException(String module,String message) {
        super(message);
        this.module = module;
    }
    public MaxUploadSizeExceededException() {
    }

    public String getCode() {
        return module;
    }

    public void setCode(String code) {
        this.module = code;
    }
}
