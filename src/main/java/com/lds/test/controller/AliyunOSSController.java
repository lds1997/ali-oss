package com.lds.test.controller;

import com.alibaba.fastjson.JSON;
import com.lds.test.domain.CommonResult;
import com.lds.test.domain.OssCallbackResult;
import com.lds.test.domain.OssPolicyResult;
import com.lds.test.domain.ResultVo;
import com.lds.test.service.impl.OssServiceImpl;
import com.lds.test.util.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

/**
 * @Author lds
 * @Date 2022/08/02 10:27
 **/

@RestController
@RequestMapping("/aliyun/oss")
public class AliyunOSSController {



    @Autowired
    private OssServiceImpl ossService;






   @RequestMapping("/test")
    public String aliyunOssCall(@RequestParam(value="file") final MultipartFile templateFile,@RequestParam(value="name") String applicationName)
    {
        String s = ossService.uploadSingleImg(templateFile,applicationName);
        System.out.println(s);
        return s;
    }


    @RequestMapping("/test1")
    public String aliyunOssCall(@RequestParam(value="file") final MultipartFile[] credentialFile,@RequestParam(value="name") String applicationName)
    {
        List<String> strings = ossService.uploadMultipleImg(credentialFile, applicationName);
        System.out.println(strings);
        return "上传成功";
    }



    @RequestMapping("/test2")
    public String aliyunOssCall(String url)
    {
        ossService.deleteOneImage(url);
        return "删除成功";
    }


    @RequestMapping("/test3")
    public String aliyunOssCall(@RequestBody  List<String> url)
    {
        Boolean is = ossService.deleteMutipleImg(url);
        System.out.println("============>"+is);
        return "删除成功";
    }


}
