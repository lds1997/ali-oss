package com.lds.test.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author lds
 * @Date 2022/07/20 13:31
 **/

@RestController
public class TestController {


    private static final Logger log = LoggerFactory.getLogger(TestController.class);

    static class Person
    {
        private String name;
        private int age;
        private int salary;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public int getSalary() {
            return salary;
        }

        public void setSalary(int salary) {
            this.salary = salary;
        }

        public Person(String name, int age, int salary) {
            this.name = name;
            this.age = age;
            this.salary = salary;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    ", salary=" + salary +
                    '}';
        }
    }






    public static void main(String[] args) {

        List<Person> personList = new ArrayList<Person>();
        personList.add(new Person("张三", 8, 3000));
        personList.add(new Person("李四", 18, 5000));
        personList.add(new Person("王五", 28, 7000));
        personList.add(new Person("孙六", 38, 9000));


         //1、从员工集合中筛选出salary大于8000的员工，并放置到新的集合里。
        List<Person> collect = personList.stream().filter(p -> p.getSalary() > 1000).limit(2).collect(Collectors.toList());
        System.out.println(collect.get(1));


    }


//    public static void main(String[] args) {
//        List<Integer> list = Arrays.asList(7, 6, 9, 3, 8, 2, 1);
//
//        list.stream().filter(x->x>5).forEach(System.out::println);
//
//        // 匹配第一个
//        Optional<Integer> findFirst = list.stream().filter(x -> x > 6).findFirst();
//        // 匹配任意（适用于并行流）
//        Optional<Integer> findAny = list.parallelStream().filter(x -> x > 6).findAny();
//        // 是否包含符合特定条件的元素
//        boolean anyMatch = list.stream().anyMatch(x -> x < 6);
//        System.out.println("findAny===========>"+findAny.get());
//    }
//


//    public static void main(String[] args) {
//        Integer a=10;
//        List<String> list = Arrays.asList("zhangsan", "lisi", "wangwu", "sunliu");
//        Comparator<? super String> comparator = Comparator.comparing(String::length);
//        Optional<String> max = list.stream().max(comparator);
//        System.out.println(max.get());
//
//    }


    public static void main(String[] args) {
        Comparator<>
    }


}
