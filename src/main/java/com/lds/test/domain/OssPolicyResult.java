package com.lds.test.domain;

/**
 * @Author lds
 * @Date 2022/08/02 10:35
 * @Describe 获取OSS上传文件授权返回结果
 **/

public class OssPolicyResult {

    private String accessKeyId;
    private String policy;
    private String signature;
    private String dir;
    private String host;
    private String callback;

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }


    public OssPolicyResult(String accessKeyId, String policy, String signature, String dir, String host, String callback) {
        this.accessKeyId = accessKeyId;
        this.policy = policy;
        this.signature = signature;
        this.dir = dir;
        this.host = host;
        this.callback = callback;
    }

    public OssPolicyResult() {
    }
}
