package com.lds.test.domain;

/**
 * @Author lds
 * @Date 2022/08/02 10:39
 * @Describe oss上传成功后的回调参数
 **/

public class OssCallbackParam {

    private String callbackUrl;
    private String callbackBody;
    private String callbackBodyType;

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCallbackBody() {
        return callbackBody;
    }

    public void setCallbackBody(String callbackBody) {
        this.callbackBody = callbackBody;
    }

    public String getCallbackBodyType() {
        return callbackBodyType;
    }

    public void setCallbackBodyType(String callbackBodyType) {
        this.callbackBodyType = callbackBodyType;
    }


    public OssCallbackParam(String callbackUrl, String callbackBody, String callbackBodyType) {
        this.callbackUrl = callbackUrl;
        this.callbackBody = callbackBody;
        this.callbackBodyType = callbackBodyType;
    }

    public OssCallbackParam() {
    }
}
