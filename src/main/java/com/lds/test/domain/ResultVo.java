package com.lds.test.domain;

/**
 * @Author lds
 * @Date 2022/08/02 10:58
 * @Describe
 **/

public class ResultVo {

    private int code;
    private String message;
    private Object result;



    public ResultVo(Object result) {
        this.code=200;
        this.result=result;

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }




}
