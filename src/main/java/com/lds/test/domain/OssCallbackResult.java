package com.lds.test.domain;

/**
 * @Author lds
 * @Date 2022/08/02 10:41
 * @Describe oss上传文件的回调结果
 **/

public class OssCallbackResult {

    private String filename;
    private String size;
    private String mimeType;
    private String width;
    private String height;


    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public OssCallbackResult(String filename, String size, String mimeType, String width, String height) {
        this.filename = filename;
        this.size = size;
        this.mimeType = mimeType;
        this.width = width;
        this.height = height;
    }

    public OssCallbackResult() {
    }
}
