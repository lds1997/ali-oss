package com.lds.test.util;


import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/** 
 * @author: nzx
 * @date: 2016年10月12日 下午3:55:06
 * @since 1.0
 */
public class HttpUtil {
	
	private static CloseableHttpClient client;
	

	 private final static PoolingHttpClientConnectionManager POOLCONNMANAGER = new PoolingHttpClientConnectionManager();
	  
	 //类加载的时候 设置最大连接数 和 每个路由的最大连接数 
	 static { 
		 POOLCONNMANAGER.setMaxTotal(500);
		 POOLCONNMANAGER.setDefaultMaxPerRoute(100); 
	 }

	
	/***
	 * 发送带语言的请求,在header中添加了locale=zh_cn标记语言国家
	 * @param url
	 * @param body
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public static String sendDataUTF8(String url, String body,Locale locale,HttpHost proxy) throws Exception{
		
		return sendData(url,body,"utf-8",locale,proxy,null);
	}
	
	public static String sendDataUTF8(String url, String body) throws Exception{
		
		return sendData(url,body,"utf-8",null,null,null);
	}

	public static String sendDataUTF8(String url, String body,HttpHost proxy) throws Exception{
		
		return sendData(url,body,"utf-8",null,proxy,null);
	}

	public static String sendDataUTF8(String url, String body,Map<String,String> header) throws Exception{

        return sendData(url,body,"utf-8",null,null,header);
    }

	/**
	 * 发送带安全令牌URL
	 * @param url
	 * @param body
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public static String sendDataUTF8(String url, String body,Locale locale,HttpHost proxy,Map<String,String> map) throws Exception{

		return sendData(url,body,"utf-8",null,null,map);
	}
	/**
	 * 发送带语言的请求,在header中添加了locale=zh_cn标记语言国家
	 * @param url
	 * @param body
	 * @param charset
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public static String sendData(String url, String body,String charset,Locale locale,HttpHost proxy,Map<String,String> map) throws Exception{
		
		String retStr = null;
		//CloseableHttpClient client = HttpClients.createDefault();
		if (client == null) {
			client = HttpClients.custom()
					.setConnectionManager(POOLCONNMANAGER)
					.evictExpiredConnections()
					.evictIdleConnections(20, TimeUnit.SECONDS)
					.build();
		}
		
		HttpPost httpPost = new HttpPost(url);
		
		StringEntity se = new StringEntity(body,charset);
		
		httpPost.setEntity(se);
		
		RequestConfig requestConfig = null;
		if(proxy != null){
			requestConfig = RequestConfig.custom().setProxy(proxy).setSocketTimeout(10000).setConnectTimeout(10000).build();//设置请求和传输超时时间
		}else{
			requestConfig = RequestConfig.custom().setSocketTimeout(10000).setConnectTimeout(10000).build();//设置请求和传输超时时间
		}
		httpPost.setConfig(requestConfig);
		
		if(locale != null){
			httpPost.setHeader("locale", locale.getLanguage() + "_" + locale.getCountry());
		}
		httpPost.addHeader("Content-Type", "application/json");
		if(map != null){
			for(String key:map.keySet()){
				httpPost.addHeader(key, map.get(key));
			}
		}
		
		HttpResponse response = client.execute(httpPost);
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			retStr = EntityUtils.toString(entity, charset);
        }
		EntityUtils.consumeQuietly(entity);
		
		return retStr;
	}
	/**
	 * 向指定URL发送GET方法的请求
	 *
	 * @param url
	 *            发送请求的URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return URL 所代表远程资源的响应结果
	 */
	public static String sendGet(String url, String param) {
		String result = "";
		BufferedReader in = null;
		try {
			String urlNameString = url + param;
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			connection.setRequestProperty("Accept-Charset", "utf-8");
			connection.setRequestProperty("contentType", "utf-8");
			// 建立实际的连接
			connection.connect();
			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			/*for (String key : map.keySet()) {
				System.out.println(key + "--->" + map.get(key));
			}*/
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送GET请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}

	public static String doGet(String path) {
		StringBuilder str = new StringBuilder();
		try {
			URL url = new URL(path);
			//打开和url之间的连接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			PrintWriter out = null;

			/**设置URLConnection的参数和普通的请求属性****start***/

			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");

			/**设置URLConnection的参数和普通的请求属性****end***/

			//设置是否向httpUrlConnection输出，设置是否从httpUrlConnection读入，此外发送post请求必须设置这两个
			//最常用的Http请求无非是get和post，get请求可以获取静态页面，也可以把参数放在URL字串后面，传递给servlet，
			//post与get的 不同之处在于post的参数不是放在URL字串里面，而是放在http请求的正文内。
			conn.setDoOutput(true);
			conn.setDoInput(true);

			conn.setRequestMethod("GET");//GET和POST必须全大写
			/**GET方法请求*****start*/
			/**
			 * 如果只是发送GET方式请求，使用connet方法建立和远程资源之间的实际连接即可；
			 * 如果发送POST方式的请求，需要获取URLConnection实例对应的输出流来发送请求参数。
			 * */
			conn.connect();

			/**GET方法请求*****end*/

			/***POST方法请求****start*/

            /*out = new PrintWriter(conn.getOutputStream());//获取URLConnection对象对应的输出流

            out.print(data);//发送请求参数即数据

            out.flush();//缓冲数据
            */

			//获取URLConnection对象对应的输入流
			InputStream is = conn.getInputStream();
			//构造一个字符流缓存
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			String line = br.readLine();
			line = new String(line.getBytes() , "utf-8" );
			//实现将字符串转成gbk类型显示.
			while(line != null ){
				str.append(line +"\r\n" );
				System.out.println(line );
				line = br.readLine();
			}

			//关闭流
			is.close();
			//断开连接，最好写上，disconnect是在底层tcp socket链接空闲时才切断。如果正在被其他线程使用就不切断。
			//固定多线程的话，如果不disconnect，链接会增多，直到收发不出信息。写上disconnect后正常一些。
			conn.disconnect();
		}catch (Exception e){
			e.printStackTrace();
		}
		return str.toString();
	}


	public static String sendHlDataUTF8(String url, String body,Map<String,String> header) throws Exception{

		return sendHlData(url,body,"utf-8",null,header);
	}


	/**
	 * 发送带语言的请求,在header中添加了locale=zh_cn标记语言国家
	 * @param url
	 * @param body
	 * @param charset
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public static String sendHlData(String url, String body,String charset,Locale locale,Map<String,String> header) throws Exception{

		String retStr = null;
		CloseableHttpClient client = null;
		client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);

		StringEntity se = new StringEntity(body,charset);

		httpPost.setEntity(se);

		RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30000).setConnectTimeout(30000).build();//设置请求和传输超时时间
		httpPost.setConfig(requestConfig);
		if(locale != null){
			httpPost.setHeader("locale", locale.getLanguage() + "_" + locale.getCountry());
		}
		if(header != null){
			for(String key : header.keySet()){
				httpPost.setHeader(key, header.get(key));
			}
		}

		HttpResponse response = client.execute(httpPost);
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			retStr = EntityUtils.toString(entity, charset);
		}
		EntityUtils.consumeQuietly(entity);

		return retStr;
	}

	/**
	 * 向指定URL发送GET方法的请求
	 *
	 * @param url
	 *            发送请求的URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return URL 所代表远程资源的响应结果
	 */
	public static String sendGetWithToken(String url, String param,String token) {
		String result = "";
		BufferedReader in = null;
		try {
			String urlNameString = url + param;
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			connection.setRequestProperty("Accept-Charset", "utf-8");
			connection.setRequestProperty("contentType", "utf-8");
			connection.addRequestProperty("Authorization", token);
			// 建立实际的连接
			connection.connect();
			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			/*for (String key : map.keySet()) {
				System.out.println(key + "--->" + map.get(key));
			}*/
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送GET请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}
}
