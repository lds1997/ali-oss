package com.lds.test.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author lds
 * @Date 2022/08/02 16:45
 * @Describe
 **/

public class StringUtil {



    public static String formatDate(Date date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(date);
    }

    public final static String getCurDate(String format) {
        Calendar cal = Calendar.getInstance();
        return formatDate(cal.getTime(), format);
    }


    public static Date parserDate(String dateStr, String format)
            throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(dateStr);
    }


}
