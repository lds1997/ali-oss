package com.lds.test.service.impl;


import com.aliyun.oss.*;
import com.aliyun.oss.model.*;
import com.lds.test.exception.MaxUploadSizeExceededException;
import com.lds.test.service.OssService;
import com.lds.test.util.StringUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.util.*;

/**
 * @Author lds
 * @Date 2022/08/02 10:45
 * @Describe oss上传管理Service实现类
 **/
@Service
public class OssServiceImpl implements OssService {


    private static final Logger LOGGER = LoggerFactory.getLogger(OssServiceImpl.class);


    @Value("${aliyun.oss.maxSize}")
    private int aliyunOssMaxSize;

    @Value("${aliyun.oss.bucketName}")
    private String aliyunOssBucketName;

    @Value("${aliyun.oss.endpoint}")
    private String aliyunOssEndpoint;

    @Value("${aliyun.oss.accessKeyId}")
    private String accessKeyId;

    @Value("${aliyun.oss.accessKeySecret}")
    private String accessKeySecret;

    @Value("${aliyun.oss.expirationDate}")
    private String expirationDate;

    @Autowired
    private OSSClient ossClient;


    /**
     * 创建OSSClient实例
     * @return
     */
    private OSSClient getOSSClient() {
        // 创建OSSClient实例
        return  new OSSClient(aliyunOssEndpoint, accessKeyId, accessKeySecret, new ClientConfiguration());
    }




    /**
     * 获得url链接
     *
     * @param key
     * @return
     */
    private String getFileUrl(OSSClient ossClient, String key) {
        try {
            URL url = ossClient.generatePresignedUrl(aliyunOssBucketName, key, StringUtil.parserDate(expirationDate, "yyyyMMdd"));
            if (url != null) {
                //把http换成https
                url.toString().replaceFirst("http", "https");
                //去掉问号后面有效期和签名信息
                String[] split = url.toString().split("\\?");
                return split[0];
            }
        } catch (ClientException e) {
            LOGGER.error("获得图片的url出错：{}", e.getMessage(), e);
        } catch (ParseException e) {
            LOGGER.error("解析过期日期失败：{}", e.getMessage(), e);
        }
        return null;
    }



    public String uploadOneImage(MultipartFile multipartFile,String applicationImage) {
        InputStream is =null;

        try {
            is = multipartFile.getInputStream();
        }catch (IOException e) {
            LOGGER.warn("上传图片出错：{}", e.getMessage(), e);
        }

        if (null == is) {
            LOGGER.info("文件对象为空，上传图片失败");
            return null;
        } else {
            String nowDay = StringUtil.getCurDate("yyyy-MM-dd");
            //拿到.号后面的文件格式类型,XXX.jpg
            //获取上传的文件的名字
            String filename = multipartFile.getOriginalFilename();
            //获得后缀.png
            String fileSuffix = filename.substring(multipartFile.getOriginalFilename().lastIndexOf("."));

            String key = applicationImage + "/" + nowDay + "/" + StringUtil.getCurDate("yyyyMMddHHmmss") + "-" + RandomStringUtils.randomNumeric(6) +fileSuffix;

            OSSClient ossClient = null;
            //上传图片
            try {
                ossClient = getOSSClient();
                //如果没有就新建一个bucket
                if (!ossClient.doesBucketExist(aliyunOssBucketName)) {
                    ossClient.createBucket(aliyunOssBucketName);
                    CreateBucketRequest createBucketRequest = new CreateBucketRequest(aliyunOssBucketName);
                    //ACL权限为公共读
                    createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
                    ossClient.createBucket(createBucketRequest);
                }
                ossClient.putObject(new PutObjectRequest(aliyunOssBucketName, key, is));

                return getFileUrl(ossClient, key);
            } catch (Exception e) {
                LOGGER.warn("保存图片出错了：{}", e.getMessage(), e);
                return null;
            } finally {
                if (ossClient != null) {
                    ossClient.shutdown();
                }
            }
        }
    }


    /**
     * 上传单张图片
     * @param multipartFile
     * @return
     */
    @Override
    public String uploadSingleImg(MultipartFile multipartFile,String applicationName) {
        if (null != multipartFile) {
            if (multipartFile.getSize() > aliyunOssMaxSize) {
                throw new MaxUploadSizeExceededException("uploadFile","文件超出最大上传限制");
            }
            return uploadOneImage(multipartFile,applicationName);
        }
        return null;
    }



    /**
     * 上传多张图片
     * @param multipartFiles
     * @return
     */
    @Override
    public List<String> uploadMultipleImg(MultipartFile[] multipartFiles,String applicationName) {
         List<String> lists=new ArrayList<>();
        //上传证件图片
        if (multipartFiles != null) {
            for (int i = 0; i < multipartFiles.length; i++) {
                try {
                    //如果文件大小为0，不做上传
                    if (multipartFiles[i].getSize() > 0) {
                        String url = uploadSingleImg(multipartFiles[i], applicationName);
                        lists.add(url);
                    }
                } catch (Exception e) {
                    LOGGER.warn("上传证件图片出错：{}", e.getMessage(), e);
                }
            }
            return lists;
        }
        return lists;
    }


    /**
     * 删除单个文件
     * 填写文件名。文件名包含路径，不包含Bucket名称
     * 例如2021/09/14/52c6a3114e634979a2934f1ea12deaadfile.png
     * @param url
     */
    @Override
    public Boolean deleteOneImage(String url) {
        boolean deleteFlag=false;
        if(url==null&&url.isEmpty()){
            LOGGER.info("图片路径url为空，删除失败");
            return false;
        }else{
            OSSClient ossClient = null;
            //删除文件
            try {
                ossClient = getOSSClient();
                ossClient.deleteObject(aliyunOssBucketName, url);
            } catch (Exception e) {
                LOGGER.warn("保存图片出错了：{}", e.getMessage(), e);
            } finally {
                if (ossClient != null) {
                    ossClient.shutdown();
                }
            }
        }
        return deleteFlag;
    }



    /**
     * 删除多个文件
     * @param urlList: ["embc-bmp/2022-08-03/20220803132758-626605.jpg","embc-bmp/2022-08-03/20220803132828-521688.jpg"]
     * return true为已删除，false为未删除
     */
    @Override
    public Boolean deleteMutipleImg(List<String> urlList) {
        boolean deleteFlag=false;
        if (urlList == null || urlList.isEmpty()) {
            LOGGER.info("图片路径List为空，批量删除图片结束");
            return false;
        } else {
            try {
                ossClient = getOSSClient();
                DeleteObjectsRequest request = new DeleteObjectsRequest(aliyunOssBucketName);
                request.setKeys(urlList);
                // 发起deleteObjects请求。
                ossClient.deleteObjects(request);
                deleteFlag=true;
                return deleteFlag;
            } catch (Exception oe) {
                LOGGER.error("批量删除图片出现错误，删除失败:{}", oe.getMessage(), oe);
            } finally {
                if (null != ossClient) {
                    if (ossClient != null) {
                        ossClient.shutdown();
                    }
                }
            }
        }
        return deleteFlag;

    }





}
