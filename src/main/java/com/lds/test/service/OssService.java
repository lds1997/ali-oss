package com.lds.test.service;

import com.lds.test.domain.OssCallbackResult;
import com.lds.test.domain.OssPolicyResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author lds
 * @Date 2022/08/02 10:44
 * @Describe oss上传管理Service
 **/
public interface OssService {


    /**
     * 上传单个文件
     * @param multipartFile 文件
     * @param applicationName 微服务名称:embc-bmp
     * @return 上传成功的图片的url:http://lds-embc.oss-cn-hangzhou.aliyuncs.com/embc-acl/2022-08-03/20220803111758-727797.jpg
     */
    public String uploadSingleImg(final MultipartFile multipartFile,String applicationName);


    /**
     * 上传多个文件
     * @param multipartFiles 文件
     * @param applicationName 微服务名称:embc-bmp
     * @return ["url1", "url2"]
     */
    public List<String> uploadMultipleImg(final MultipartFile[] multipartFiles,String applicationName);



    /**
     * 删除单个文件
     * 填写文件名。文件名包含路径，不包含Bucket名称
     * @param url: embc-bmp/2022-08-03/20220803132758-626605.jpg
     * return true为已删除，false为未删除
     */
    public Boolean deleteOneImage(final String url);


    /**
     * 删除多个文件
     * @param urlList: ["embc-bmp/2022-08-03/20220803132758-626605.jpg","embc-bmp/2022-08-03/20220803132828-521688.jpg"]
     * return true为已删除，false为未删除
     */
    public Boolean deleteMutipleImg(final List<String> urlList);


}
